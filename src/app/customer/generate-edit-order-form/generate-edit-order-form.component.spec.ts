import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateEditOrderFormComponent } from './generate-edit-order-form.component';

describe('GenerateEditOrderFormComponent', () => {
  let component: GenerateEditOrderFormComponent;
  let fixture: ComponentFixture<GenerateEditOrderFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateEditOrderFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateEditOrderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
