//import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import * as myGlobals from "../../api-module/services/globalPath";
import { Http, RequestOptions, Headers } from "@angular/http";

@Injectable()
export class UserService {

  constructor(private readonly http: Http) { }
  
  updateUserPassword(model): Observable<any> {
    var serviceBaseUrl = myGlobals.BaseUrlUserManagement;
    const url = serviceBaseUrl + "User/UpdateUserPassword";
    
    return this.http.put(url, model, this.jwt()).pipe(map(
      data => {
        const result = <any>data;
        return result;
      },
      error => {
        console.log(JSON.stringify(error.json()));
      }));
  }

    private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem("currentUser"));
    if (currentUser && currentUser.token.accessToken) {
      let headers = new Headers({
        Authorization: "bearer " + currentUser.token.accessToken
      });
      headers.append("Content-Type", "application/json");
      return new RequestOptions({ headers: headers });
    }
  }

}

