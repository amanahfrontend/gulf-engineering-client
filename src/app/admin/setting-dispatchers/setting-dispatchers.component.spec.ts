import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingDispatchersComponent } from './setting-dispatchers.component';

describe('SettingDispatchersComponent', () => {
  let component: SettingDispatchersComponent;
  let fixture: ComponentFixture<SettingDispatchersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingDispatchersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingDispatchersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
